package be.intecbrussel;

import java.util.Random;

public class PizzaShop implements Runnable {

    private double income;
    private PizzaStorage pizzaStorage;

    public PizzaShop(PizzaStorage pizzaStorage){
        setPizzaStorage(pizzaStorage);
    }

    public void sellPizza(Pizza pizza){
        if (pizzaStorage != null) {
            setIncome(getIncome() + pizzaStorage.orderPizza(pizza).getPrice());
        }
    }

    public PizzaStorage getPizzaStorage() {
        return pizzaStorage;
    }

    public void setPizzaStorage(PizzaStorage pizzaStorage) {
        this.pizzaStorage = pizzaStorage;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    @Override
    public void run() {
        for (int i = 0; i < 50; i++) { //aanpassen om minder pizza's te verkopen per shop
            Random rand = new Random();
            int x = rand.nextInt(4);
            switch (x) {
                case 0:
                    sellPizza(new Pizza(PizzaType.STANDARD));
                    break;
                case 1:
                    sellPizza(new Pizza(PizzaType.DEEP_DISH));
                    break;
                case 2:
                    sellPizza(new Pizza(PizzaType.CALZONE));
                    break;
                case 3:
                    sellPizza(new Pizza(PizzaType.CHEESE_CRUST));
                    break;
            }
            try {
                Thread.sleep(5000);  //aanpassen om sneller te laten lopen
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
