package be.intecbrussel;

public enum Ingredient {
    TOMATOSAUCE,
    CHEESE,
    PEPPERS,
    MUSHROOMS,
    PEPPERONI,
    ONION,
    MEATBALLS,
    HAM,
    MOZARELLA;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
