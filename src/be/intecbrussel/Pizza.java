package be.intecbrussel;

import java.util.ArrayList;
import java.util.Random;

public class Pizza {

    private PizzaType pizzaType;
    private double price;
    private ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();


    public Pizza(PizzaType pizzaType){
        setPizzaType(pizzaType);
        setPrice(pizzaType.getPrice());
        getIngredients().add(Ingredient.TOMATOSAUCE);
        addRandomIngredients();
    }

    private void addRandomIngredients() {
        Random rand = new Random();
        int amountOfIngredients = rand.nextInt(3) + 1;
        for (int i = 0; i < amountOfIngredients; i++){
            int ingredientIndex = rand.nextInt(Ingredient.values().length);
            if (!getIngredients().contains(Ingredient.values()[ingredientIndex])){
                getIngredients().add(Ingredient.values()[ingredientIndex]);
            }
        }
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }


    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        return pizzaType + " pizza" +
                ": price: €" + price +
                ", ingredients: " + ingredients;
    }
}
