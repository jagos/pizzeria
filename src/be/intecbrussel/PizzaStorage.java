package be.intecbrussel;

import java.util.*;

public class PizzaStorage {

    private Map<PizzaType,List<Pizza>> storage = new HashMap<>();
    private boolean open = false;

    public PizzaStorage() {}

    public synchronized void storePizza(Pizza pizza){
        storage.computeIfAbsent(pizza.getPizzaType(), k -> new ArrayList<>()).add(pizza);
        checkIfOpen();
    }
    
    private boolean inStock(Pizza pizza){
        if (storage != null) {
            storage.computeIfAbsent(pizza.getPizzaType(), k -> new ArrayList<>());
        }
        return storage.get(pizza.getPizzaType()).size() > 0;
    }

    public synchronized Pizza orderPizza(Pizza pizza){
        if (storage != null) {
            if (inStock(pizza)) {
                storage.get(pizza.getPizzaType()).remove(0);
                System.out.println("sold " + pizza.toString() + ". " + storage.get(pizza.getPizzaType()).size() + " left in stock.");
                checkIfOpen();
            } else {
                storage.put(pizza.getPizzaType(), new ArrayList<>());
                for (Map.Entry<PizzaType, List<Pizza>> entry : storage.entrySet()) {
                    if (entry.getValue().size() > 0) {
                        Pizza newPizza = new Pizza(entry.getKey());
                        newPizza.setPrice(newPizza.getPrice() / 2);
                        entry.getValue().remove(0);
                        checkIfOpen();
                        System.out.println(pizza.getPizzaType().toString() + " out of stock. You receive the following pizza for half price: " +
                                newPizza.getPizzaType().toString() + ", €" + newPizza.getPrice() + " " + entry.getValue().size() + " left in stock.");
                        return newPizza;
                    }
                }
            }
        }

        return pizza;
    }

    public synchronized boolean checkIfOpen(){
        int count = 0;
        if (storage != null) {
            for (Map.Entry<PizzaType, List<Pizza>> entry : storage.entrySet()) {
                if (entry.getValue().size() > 0) {
                    count++;
                }
            }
            if (count > (storage.size() / 2)) {
                setOpen(true);
            } else {
                setOpen(false);
            }
        }
        return getOpen();
    }

    public  Map<PizzaType, List<Pizza>> getStorage() {
        return storage;
    }

    private void setOpen(boolean open) {
        this.open = open;
    }

    private boolean getOpen(){
        return open;
    }
}