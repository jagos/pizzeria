package be.intecbrussel;

public class App {
    public static void main(String[] args) throws InterruptedException {
        PizzaStorage pizzaStorage = new PizzaStorage();
        PizzaFactory pizzaFactory1 = new PizzaFactory(pizzaStorage);
        PizzaFactory pizzaFactory2 = new PizzaFactory(pizzaStorage);
        PizzaShop pizzaShop1 = new PizzaShop(pizzaStorage);
        PizzaShop pizzaShop2 = new PizzaShop(pizzaStorage);
        PizzaShop pizzaShop3 = new PizzaShop(pizzaStorage);
        PizzaShop pizzaShop4 = new PizzaShop(pizzaStorage);

        Thread factoryThread1 = new Thread(pizzaFactory1);
        Thread factoryThread2 = new Thread(pizzaFactory2);
        factoryThread1.setDaemon(true);
        factoryThread2.setDaemon(true);
        factoryThread1.setName("Pizzafactory1");
        factoryThread2.setName("Pizzafactory2");
        factoryThread1.start();

        factoryThread2.start();

        Thread shopThread1 = new Thread(pizzaShop1);
        Thread shopThread2 = new Thread(pizzaShop2);
        Thread shopThread3 = new Thread(pizzaShop3);
        Thread shopThread4 = new Thread(pizzaShop4);
        shopThread1.start();
        shopThread2.start();
        shopThread3.start();
        shopThread4.start();

        shopThread1.join();
        shopThread2.join();
        shopThread3.join();
        shopThread4.join();

        double income = pizzaShop1.getIncome() + pizzaShop2.getIncome() + pizzaShop3.getIncome() + pizzaShop4.getIncome();
        System.out.println("total income: €" + income);
    }
}
