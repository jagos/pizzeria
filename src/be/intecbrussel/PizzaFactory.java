package be.intecbrussel;

import java.util.*;

public class PizzaFactory implements Runnable{

    private PizzaStorage pizzaStorage;

    public PizzaFactory(PizzaStorage pizzaStorage) {
        setPizzaStorage(pizzaStorage);
    }

    public void setPizzaStorage(PizzaStorage pizzaStorage) {
        this.pizzaStorage = pizzaStorage;
    }

    public PizzaStorage getPizzaStorage() {
        return pizzaStorage;
    }

    @Override
    public synchronized void run() {
        while (true){
            if(pizzaStorage != null) {
                Random rand = new Random();
                int amountOfPizzas;
                if (!pizzaStorage.checkIfOpen()) {
                    for (Map.Entry<PizzaType, List<Pizza>> entry : pizzaStorage.getStorage().entrySet()) {
                        amountOfPizzas = rand.nextInt(3);
                        for (int i = 0; i <= amountOfPizzas; i++) {
                            if (entry.getValue().size() <= 10) {
                                pizzaStorage.storePizza(new Pizza(entry.getKey()));
                            }
                        }
                        System.out.println(Thread.currentThread().getName() + " refilled stock with " + (amountOfPizzas + 1) + " " + entry.getKey().toString());
                    }
                }
            }
        }
    }
}