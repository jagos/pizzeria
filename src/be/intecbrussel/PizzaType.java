package be.intecbrussel;

public enum PizzaType {

    STANDARD(10),
    DEEP_DISH(12),
    CHEESE_CRUST(14),
    CALZONE(10);

    private double price;

    PizzaType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
